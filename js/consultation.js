(function ($, Drupal) {
  Drupal.behaviors.InitialiseSurvey = {
    attach: function (context, settings) {
      $('div.consultation-survey', context).once('surveyslides').each(function () {

        $(this).find('.field--name-questions > .field__item').first().addClass('current');
        $(this).find('.field--name-questions > .field__item .consultation-question-button').click(handleSurveyButtonClick);
        $(this).find('.field--name-questions > .field__item > .consultation-question.question-type-select-image-options-question input[type="checkbox"]').change(combineCheckedInputs);
        $(this).find('.field--name-questions > .field__item > .consultation-question.question-type-sorted-list-question .field--name-field-list-options > .field__item > input').each((function() {
          $(this).closest('.field__item').data('list-option-slug-value', $(this).val());
        }));
        Sortable.create($(this).find('.field--name-questions > .field__item > .consultation-question.question-type-sorted-list-question .field--name-field-list-options')[0], { easing: "cubic-bezier(1,.04,.24,1)", animation: 150, dataIdAttr: 'list-option-slug-value', onSort: function() {combineSortedQuestionAnswer.call(this.el); }})
        $(this).find('.field--name-questions > .field__item > .consultation-question.question-type-sorted-list-question .field--name-field-list-options').each(function() {
          combineSortedQuestionAnswer.call(this);
        });

      });

      function getResponseIDFromCookie() {
        if(Cookies) {
          var response_id = Cookies.get('ConsultationResponseID');
          if(!response_id)
            response_id = uuidv4();
          Cookies.set('ConsultationResponseID', response_id, 7);
          return response_id;
        }
      }

      function handleSurveyButtonClick(event) {
        var target_element = this;
        var next_function = null;
        var $question_container = $(this).closest('.consultation-question');
        if($(target_element).hasClass('consultation-question-next-button')) 
          next_function = showNextQuestion;
        else if($(target_element).hasClass('consultation-question-prev-button')) 
          next_function =  showPrevQuestion;
        else if($(target_element).hasClass('consultation-question-finish-button')) 
          next_function = endSurvey;
        submitSurveyQuestionAnswer.call($question_container, next_function);
        
      }

      function submitSurveyQuestionAnswer(next_function) {
        var $question_container = this;
        $validation_error = $question_container.find('.consultation-question-validation-message');
        var survey_id =  $question_container.data('consultationSurveyId');;
        var question_id = $question_container.data('consultationQuestionId');
        var question_type = $question_container.data('consultationQuestionType');
        var response_id = getResponseIDFromCookie();
        var value = $('input[name="consultation-question-' + question_id +'"], textarea[name="consultation-question-' + question_id +'"]').val();
        var consultation_answer_posted_event = new CustomEvent('consultation_answer_posted_event', {question_detail: {value: value, question_id: question_id}});
        var consultation_answer_posted_success_event = new CustomEvent('consultation_answer_posted_success_event', {question_detail: {value: value, question_id: question_id}});
        if($('input[name="consultation-question-' + question_id +'"]').prop('type') == 'radio')
          value = $('input[name="consultation-question-' + question_id +'"]:checked').val();
        var question_type_slug = question_type.replace('question-type-', '').replace(/\-/g, '_');
        if($(this).data('consultationQuestionRequired') && !consultation_validation.required(value)) {
          $validation_error.text('You need to answer this question before you can continue with the rest of the survey');
          return;
        } 
        else {
          // VALIDATION CHECKS IN HERE
          $question_container[0].dispatchEvent(consultation_answer_posted_event);
          $validation_error.text('');
          $.post("/consultation/submit",{
            value: value, 
            question_type: question_type,
            question_id: question_id,
            survey_id: survey_id,
            response_id: response_id
          }, function(data) {
              $question_container[0].dispatchEvent(consultation_answer_posted_success_event);
              $question_container.data('consultationQuestionSubmitted', true);
              if(next_function)
                next_function.call($question_container);
          });
        }
      }

      function combineSortedQuestionAnswer() {
        var $list_container = $(this); 
        var $question_container = $list_container.closest('.consultation-question');
        var $question_container_answer_input = $question_container.find('.question-combined-answer');
        var list_values = [];
        var list_items = $list_container.children('.field__item');
        for(var i=0; i<list_items.length; i++) 
          list_values.push($(list_items[i]).data('list-option-slug-value'));
        $question_container_answer_input.val(list_values.join(','));
      }

      function combineCheckedInputs(event) { 
        var $question_container = $(this).closest('.consultation-question');
        var $question_container_answer_input = $question_container.find('.question-combined-answer');
        var answers = [];
        var check_group_name = $(this).prop('name');
        $question_container.find('input[name="' + check_group_name + '"]:checked').each(function() {
          answers.push($(this).val());
        });
        $question_container_answer_input.val(answers.join(','));
      }

      function showNextQuestion() {

        var near_survey = this.closest('.consultation-survey');

        if(!near_survey.data('started') === true) {
          near_survey.data('started', true);
          var dataLayer = window.dataLayer = window.dataLayer || [];
          dataLayer.push({
            'event': 'survey_start',
            'surveyName': near_survey.find('.field--name-name').html()
          });
        }

        this.parent().removeClass('current').next().addClass('current');
        near_survey.find('.consultation-survey-progress-index').text(near_survey.find('.field__item.current').index() + 1);
      }

      function showPrevQuestion() {
        this.parent().removeClass('current').prev().addClass('current');
        this.closest('.consultation-survey').find('.consultation-survey-progress-index').text($(this).closest('.consultation-survey').find('.field__item.current').index() + 1);
      }

      function endSurvey() {
        $consultation_survey = this.closest('.consultation-survey');
        $consultation_survey.find('.consultation-question').each(function(index, question) {
          var $question = $(question);
          if(!$question.data('consultationQuestionSubmitted'))
            submitSurveyQuestionAnswer.call($question, undefined);
        })
        $consultation_survey.find('.field--name-questions > .field__item').removeClass('current');
        $consultation_survey.find('.field--name-completion-message').addClass('show');
        $consultation_survey.find('.consultation-survey-progress-wrapper').hide();
        $consultation_survey.addClass('completed');

        var completed_event = new CustomEvent('consultation_survey_complete', {
          detail: {
            consultation_id: $consultation_survey.prop('id').replace('consultation-survey-', '')
          }
        });
        $consultation_survey[0].dispatchEvent(completed_event);

        // add to datalayer if exists
        var dataLayer = window.dataLayer = window.dataLayer || [];
        dataLayer.push({
          'event': 'survey_complete',
          'surveyName': $consultation_survey.find('.field--name-name').html()
        });
      }

    }
  };
})(jQuery, Drupal);