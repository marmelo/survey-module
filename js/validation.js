var consultation_validation = {
  options_question:               function(value) { return this.notEmpty(value) },
  select_image_options_question:  function(value) { return this.notEmpty(value) },
  slider_question:                function(value) { return this.notEmpty(value) },
  sorted_list_question:           function(value) { return this.notEmpty(value) },
  text_question:                  function(value) { return this.notEmpty(value) },
  default:                        function(value) { return this.notEmpty(value) },
  required:                       function(value) { return this.notEmpty(value) },
  notEmpty:                       function (value) { return !!value; }
};

