<?php

namespace Drupal\consultation;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Provides an interface for defining Survey Question Type entity type entities.
 */
interface SurveyQuestionTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface {}