<?php

namespace Drupal\consultation;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Survey Answer entity.
 * @ingroup consultation
 */
interface SurveyAnswerInterface extends ContentEntityInterface, EntityChangedInterface {

}