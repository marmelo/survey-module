<?php

namespace Drupal\consultation\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\consultation\SurveyQuestionTypeInterface;

/**
 * Survey Question Type
 * 
 * @ConfigEntityType(
 *   id = "consultation_question_type",
 *   label = @Translation("Question Type"),
 *   bundle_of = "consultation_question",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_prefix = "consultation_question_type",
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   },
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\consultation\Entity\Controller\SurveyQuestionTypeListBuilder",
 *     "form" = {
 *       "default" = "Drupal\consultation\Form\SurveyQuestionTypeForm",
 *       "add" = "Drupal\consultation\Form\SurveyQuestionTypeForm",
 *       "edit" = "Drupal\consultation\Form\SurveyQuestionTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer site configuration",
 *   links = {
 *     "canonical" = "/admin/structure/question_types/{consultation_question_type}",
 *     "add-form" = "/admin/structure/question_types/add",
 *     "edit-form" = "/admin/structure/question_types/{consultation_question_type}/edit",
 *     "delete-form" = "/admin/structure/question_types/{consultation_question_type}/delete",
 *     "collection" = "/admin/structure/question_types",
 *   },
 *   field_ui_base_route = "entity.consultation_question_type.edit_form",
 * )
 */

class SurveyQuestionType extends ConfigEntityBundleBase implements SurveyQuestionTypeInterface{
  /**
   * The machine name of the survey question type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the survey question type.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of the survey question type.
   *
   * @var string
   */
  protected $description;
  
  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }  
}
