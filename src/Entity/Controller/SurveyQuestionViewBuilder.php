<?php

namespace Drupal\consultation\Entity\Controller;

use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Entity\EntityInterface;

class SurveyQuestionViewBuilder extends EntityViewBuilder {

  public function view(EntityInterface $entity, $view_mode = 'full', $langcode = NULL) {
    $view = parent::view($entity, $view_mode, $langcode);
    return $view;
  }

  protected function getBuildDefaults(EntityInterface $entity, $view_mode) {
    $build = parent::getBuildDefaults($entity, $view_mode);
    return $build;
  }
  

}