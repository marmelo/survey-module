<?php

namespace Drupal\consultation\Entity\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Url;

/**
 * Provides a list controller for consultation_survey entity.
 *
 * @ingroup consultation
 */
class SurveyListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   *
   * We override ::render() so that we can add our own content above the table.
   * parent::render() is where EntityListBuilder creates the table using our
   * buildHeader() and buildRow() implementations.
   */
  public function render() {
    $build['description'] = [
      '#markup' => $this->t('List of surveys. You can manage the fields on the <a href="@adminlink">Survey settings page</a>.', array(
        '@adminlink' => \Drupal::urlGenerator()
          ->generateFromRoute('consultation_survey.settings'),
      )),
    ];

    $build += parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   *
   * Building the header and content lines for the contact list.
   *
   * Calling the parent::buildHeader() adds a column for the possible actions
   * and inserts the 'edit' and 'delete' links as defined for the entity type.
   */
  public function buildHeader() {
    
    $header['name'] = $this->t('Name');
    $header['author'] = $this->t('Author');
    $header['updated'] = $this->t('Updated');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\content_entity_example\Entity\Contact */
    
    $row['name'] = $entity->get('name')->value;
    $row['author'] = $entity->get('user_id')->first()->get('entity')->getTarget()->getValue()->get('name')->value;
    $row['updated'] = \Drupal::service('date.formatter')->format($entity->get('changed')->value);
    return $row + parent::buildRow($entity);
  }

}
?>