<?php

namespace Drupal\consultation\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\consultation\SurveyAnswerInterface;

/**
 * Defines the Consultation Survey entity.
 *
 * @ingroup consultation
 *
 * @ContentEntityType(
 *   id = "consultation_answer",
 *   label = @Translation("Survey answer entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
  *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\consultation\SurveyAnswerAccessControlHandler",
 *   },
 *   base_table = "consultation_answer",
 *   admin_permission = "administer survey",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *   },
 * )
 *
 *
 */

class SurveyAnswer extends ContentEntityBase implements SurveyAnswerInterface {

  use  EntityChangedTrait;

 /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Quiz entity.'))
      ->setReadOnly(TRUE);



    $fields['survey_question'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Question'))
      ->setDescription(t('The question'))
      ->setSetting('target_type', 'consultation_question')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'entity_reference_label',
        'weight' => -3,
      ))
      ->setDisplayOptions('form', array(
         'type'     => 'entity_reference_autocomplete',
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
        'weight' => -3,
      ))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

      $fields['survey'] = BaseFieldDefinition::create('entity_reference')
        ->setLabel(t('Survey'))
        ->setDescription(t('The Survey'))
        ->setSetting('target_type', 'consultation_survey')
        ->setSetting('handler', 'default')
        ->setDisplayOptions('view', array(
          'label' => 'hidden',
          'type' => 'entity_reference_label',
          'weight' => -3,
        ))
        ->setDisplayOptions('form', array(
          'type'     => 'entity_reference_autocomplete',
          'settings' => array(
            'match_operator' => 'CONTAINS',
            'size' => 60,
            'autocomplete_type' => 'tags',
            'placeholder' => '',
          ),
          'weight' => -3,
        ))
        ->setDisplayConfigurable('form', FALSE)
        ->setDisplayConfigurable('view', FALSE);

    $fields['answer_value'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Answer value'))
      ->setDescription(t('The submitted response to the question'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['answer_weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Answer weight'))
      ->setDescription(t('The weight of the submitted response'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);
    
    $fields['response_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Response ID'))
      ->setDescription(t('Unique ID linking survey responses from one user'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code of Quiz entity.'));
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}