<?php

namespace Drupal\consultation\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\consultation\SurveyInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Consultation Survey entity.
 *
 * @ingroup consultation
 *
 * @ContentEntityType(
 *   id = "consultation_survey",
 *   label = @Translation("Consultation survey"),
 *   label_collection = @Translation("Consultation survey list"),
 *   label_singular = @Translation("Consultation survey"),
 *   label_plural = @Translation("Consultation surveys"),
 *   label_count = @PluralTranslation(
 *     singular = "@count consultation survey",
 *     plural = "@count consultation surveys",
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\consultation\Entity\Controller\SurveyListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\consultation\Form\SurveyForm",
 *       "edit" = "Drupal\consultation\Form\SurveyForm",
 *       "delete" = "Drupal\consultation\Form\SurveyDeleteForm",
 *     },
 *     "access" = "Drupal\consultation\SurveyAccessControlHandler",
 *   },
 *   base_table = "consultation_survey",
 *   admin_permission = "administer survey",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *   },
 *   links = {
 *     "canonical" = "/surveys/{consultation_survey}",
 *     "edit-form" = "/surveys/{consultation_survey}/edit",
 *     "delete-form" = "/surveys/{consultation_survey}/delete",
 *     "collection" = "/admin/content/surveys"
 *   },
 *   field_ui_base_route = "consultation_survey.settings",
 * )
 *
 *
 */

class Survey extends ContentEntityBase implements SurveyInterface {

  use  EntityChangedTrait;

  /**
   *  {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

 /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Quiz entity.'))
      ->setReadOnly(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Survey entity.'))
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -6,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -6,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    
    $fields['questions'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Questions'))
      ->setDescription(t('Questions in the survey'))
      ->setSetting('target_type', 'consultation_question')
      ->setSetting('handler', 'default')
      ->setSetting('handler_settings', ['target_bundles' => [
        'text_question' => 'text_question', 
        'slider_question' => 'slider_question',
        'options_question' => 'options_question',
        'select_image_options_question' => 'select_image_options_question',
        'sorted_list_question' => 'sorted_list_question',
        'short_text_question' => 'short_text_question'
      ]])
      ->setCardinality(\Drupal\Core\Field\FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'entity_reference_label',
        'weight' => -3,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'inline_entity_form_complex',
        'settings' => array(
          'label_singular' => 'Question',
          'label_plural' => 'Questions',
          'allow_new' => true,
          'allow_existing' => false,
          'match_operator' => 'CONTAINS',
          'collapsible' => true,
          'collapsed' => true,
        ),
        'weight' => -3,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['completion_message'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Completion message'))
      ->setDescription(t('This message is displayed when the user finishes the survey.'))
      ->setSettings(array(
        'default_value' => '',
        'text_processing' => 0,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -6,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'text_textarea',
        'settings' => array(
          'rows' => 7
        )
        ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    
    $fields['submit_text'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Final survey Submission button text'))
      ->setDescription(t('This is the text used on the final button that submits the survey.'))
      ->setSettings(array(
        'default_value' => 'Finish',
        'max_length' => 255,
        'text_processing' => 0,
      ))
      ->setDisplayOptions('view', array(
        'region' => 'hidden'
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 1,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // $fields['Questions'] = BaseFieldDefinition::create('entity_reference')
    //   ->setLabel(t('Questions'))
    //   ->setDescription(t('The questions/result screens'))
    //   ->setSetting('target_type', 'cyo_quiz_question')
    //   ->setSetting('handler', 'default')
    //   ->setCardinality(\Drupal\Core\Field\FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
    //   ->setDisplayOptions('view', array(
    //     'label' => 'above',
    //     'type' => 'entity_reference_label',
    //     'weight' => -3,
    //   ))
    //   ->setDisplayOptions('form', array(
    //     'type' => 'inline_entity_form_complex',
    //     'settings' => array(
    //       'label_singular' => 'Question',
    //       'label_plural' => 'Questions',
    //       'allow_new' => true,
    //       'allow_existing' => false,
    //       'match_operator' => 'CONTAINS',
    //       'collapsible' => true,
    //       'collapsed' => true,
    //     ),
    //     // 'settings' => array(
    //     //   'match_operator' => 'CONTAINS',
    //     //   'size' => 60,
    //     //   'autocomplete_type' => 'tags',
    //     //   'placeholder' => '',
    //     // ),
    //     'weight' => -3,
    //   ))
    //   ->setDisplayConfigurable('form', TRUE)
    //   ->setDisplayConfigurable('view', TRUE);



    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User Name'))
      ->setDescription(t('The Name of the associated user.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code of Quiz entity.'));
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}