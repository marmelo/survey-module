<?php

namespace Drupal\consultation\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\consultation\SurveyQuestionInterface; 
use Drupal\user\UserInterface;

/**
 * Defines the Consultation Survey question entity.
 *
 * @ingroup consultation
 *
 * @ContentEntityType(
 *   id = "consultation_question",
 *   label = @Translation("Question entity"),
 *   base_table = "consultation_question",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "bundle",
 *     "user_id" = "user_id",
 *     "created" = "created",
 *     "changed" = "changed",
 *   },
 *   fieldable = TRUE,
 *   admin_permission = "administer survey",
 *   handlers = {
 *     "access" = "Drupal\consultation\SurveyQuestionAccessControlHandler",
 *     "view_builder" = "Drupal\consultation\Entity\Controller\SurveyQuestionViewBuilder",
 *     "form" = {
 *       "default" = "Drupal\consultation\Form\SurveyQuestionForm",
 *       "add" = "Drupal\consultation\Form\SurveyQuestionForm",
 *       "edit" = "Drupal\consultation\Form\SurveyQuestionForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   bundle_entity_type = "consultation_question_type",
 *   field_ui_base_route = "entity.consultation_question_type.canonical",
 * )
 */

class SurveyQuestion extends ContentEntityBase implements SurveyQuestionInterface {

  use  EntityChangedTrait;

   /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * The label callback for this configuration entity.
   *
   * @return string The label.
   */
  public function label() {
    $question_prefix = "New Question: ";
    if(($question_number = $this->getQuestionNumber()) !== FALSE) $question_prefix = "Question ".$question_number.": ";

    $question_text= $question_prefix.substr(strip_tags($this->get('question_text')->value), 0, 50);
    return $question_text;
  }

  public function getSurvey() {
    $survey_query = \Drupal::entityQuery('consultation_survey')
                    ->condition('questions.target_id', $this->id());
    $survey_query_result = $survey_query->execute();                    
    $survey_entity_id = reset($survey_query_result);
    if($survey_entity_id) 
      return Survey::load($survey_entity_id);
    return FALSE;
  }

  public function getQuestionNumber() {
    if($survey_entity = $this->getSurvey()) {
      $questions = $survey_entity->get('questions');
      $counter = 0;
      foreach($questions as $question) {
        $counter++;
        if($question->getValue()['target_id'] == $this->id()) return $counter;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Question entity.'))
      ->setReadOnly(TRUE);

    $fields['question_text'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Question text'))
      ->setDescription(t('The text used to ask this question to people taking the survey.'))
      ->setSettings(array(
        'default_value' => '',
        'text_processing' => 0,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -6,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'text_textarea',
        'settings' => array(
          'rows' => 4
        )
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['question_required'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Required question'))
      ->setDescription(t('Is this question mandatory?  If yes the user will not be able to progress the survey until they\'ve provided a response'))
      ->setSettings(array(
        'default_value' => FALSE,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'boolean_checkbox',
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code of Quiz entity.'));
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   *  {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

}