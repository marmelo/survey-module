<?php
namespace Drupal\consultation\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;;
use Drupal\consultation\Entity\SurveyAnswer;

class SubmitSurveyAnswerAjax extends ControllerBase {

  public function submitAnswer() {
    $request = \Drupal::request()->request;
    $value = $request->get('value');
    $question_type = $request->get('question_type');
    $question_id = $request->get('question_id');
    $survey_id = $request->get('survey_id');
    $response_id = $request->get('response_id');

    $answers = [];

    if($value && $question_type && $question_id && $survey_id && $response_id) {
      switch($question_type) {
        case 'select-image-options-question':
        case 'sorted-list-question':
          $values = explode(',', $value);
          for($i=0;$i<count($values);$i++)
            $answers[] = ['answer_value' => $values[$i], 'answer_weight' => $i];
          break;
        case 'options-question':
        case 'slider-question':
        case 'text-question':
        case 'short-text-question':
        default:
          $answers[] = ['answer_value' => $value, 'answer_weight' => 0];
      }
    
      if(count($answers)) {
        $existing_answer_query = \Drupal::entityQuery('consultation_answer')
          ->condition('response_id', $response_id)
          ->condition('survey', $survey_id)
          ->condition('survey_question', $question_id);
        $existing_answer_ids = $existing_answer_query->execute();
        $existing_answers = SurveyAnswer::loadMultiple($existing_answer_ids);
        foreach($existing_answers as $existing_answer)
          $existing_answer->delete();
        
        foreach($answers as $answer) {
          $answer['response_id'] = $response_id;
          $answer['survey_question'] = $question_id;
          $answer['survey'] = $survey_id;
          $answer_entity = SurveyAnswer::create($answer);
          $answer_entity->save();
        }
      }
    }

    $result = ['response' => 'hello'];
    return new JsonResponse($result);
  }

}


