<?php
namespace Drupal\consultation\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\consultation\Entity\Survey;
use Drupal\consultation\Entity\SurveyQuestion;
use Drupal\consultation\Entity\SurveyAnswer;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\IOFactory;

use \Symfony\Component\HttpFoundation\Response;

class Submissions extends ControllerBase {
  public function view(Survey $consultation_survey) {
    // How many rows to display per page
    $display_count = 50;
    // Okay here we go.  Start simple with a little title markup.  We get passed the survey entity and the page number by Drupal routing
    $page_response['title'] = ['#markup' => '<h2><i>'.t($consultation_survey->label().'</i> submissions</h2>')];

    $header_array = $this->buildSubmissionTableHeaders($consultation_survey);
    $answer_data = $this->getSubmissionData($consultation_survey, TRUE, 50);
    $answer_rows = $this->buildSubmissionTable($answer_data, $consultation_survey, array_column($header_array, 'id'));

    $header_labels = array_column($header_array, 'text');
    $header_labels[] = 'Date submitted';

    // Done!  Bung it all in a table.
    $page_response['submissions_table'] = [
      '#theme' => 'table',
      '#header' => $header_labels,
      '#rows' => array_values($answer_rows),
      '#empty' => 'No submissions found'
    ];

    $page_response['submissions_pager'] = [
      '#type' => 'pager'
    ];

    return $page_response;
  }

  public function export(Survey $consultation_survey) {
    $config = \Drupal::config('system.site');
    $site_name = $config->get('name');

    $header_array = $this->buildSubmissionTableHeaders($consultation_survey);
    $answer_data = $this->getSubmissionData($consultation_survey, FALSE);
    $answer_rows = $this->buildSubmissionTable($answer_data, $consultation_survey, array_column($header_array, 'id'));

    $header_labels = array_column($header_array, 'text');
    $header_labels[] = 'Date submitted';

    $spreadsheet_name = 'Consult-360_'.$site_name.'_'.$consultation_survey->label().'_'.date('Y-m-d_H-i-s');

    $row_range  = "A#:".chr(64 + count($header_labels))."#";
    $styleArrayHead = array(
      'font' => array(
        'bold' => true,
        'color' => array('rgb' => 'ffffff'),
      ));

    $spread_sheet = new SpreadSheet();
    $spread_sheet->setActiveSheetIndex(0);
    $active_sheet = $spread_sheet->getActiveSheet();
    $active_sheet->setTitle('Survey results');
    $active_sheet->getColumnDimension(str_replace('#','', $row_range))->setWidth(200);
    $spread_sheet->getDefaultStyle()->getFont()->setName('Verdana');
    $active_sheet->fromArray($header_labels, NULL, 'A1');
    $active_sheet->getStyle(str_replace('#','1', $row_range))
      ->getFont()
      ->setSize(12);
    $active_sheet->getStyle(str_replace('#','1', $row_range))
      ->getFill()
      ->setFillType(Fill::FILL_SOLID)
      ->getStartColor()
      ->setARGB('4f1a48');

    $active_sheet->getStyle(str_replace('#','1', $row_range))->getAlignment()->setWrapText(true);
    $active_sheet->getStyle(str_replace('#','1', $row_range))->applyFromArray($styleArrayHead);
    $writer = IOFactory::createWriter($spread_sheet, 'Xlsx');
    $row_counter = 2;
    foreach($answer_rows as $answer_row) 
      $active_sheet->fromArray($answer_row, NULL, 'A'.$row_counter++);
    ob_start();
    $writer->save('php://output');
    $content = ob_get_clean();
    $spread_sheet->disconnectWorksheets();
    unset($spread_sheet);

    $response = new Response();
    $response->headers->set('Pragma', 'no-cache');
    $response->headers->set('Expires', '0');
    $response->headers->set('Content-Type', 'application/vnd.ms-excel');
    $response->headers->set('Content-Disposition', 'attachment; filename='.$spreadsheet_name.'.xlsx');
    $response->setContent($content);

    return $response;
  }

  private function buildSubmissionTableHeaders(Survey $consultation_survey) {
    // Let's get the list of questions in this survey.  We construct an array contain the question ID and the cleaned up question text
    $questions = $consultation_survey->get('questions');
    $header_array = [];
    foreach($questions as $question) {
      $question_id = $question->getValue()['target_id'];
      $question = SurveyQuestion::load($question_id);
      $header_array[] = [
        'id' => $question_id,
        'text' => str_replace(array("\r\n", "\n", "\r", '&nbsp;'), '', strip_tags($question->get('question_text')[0]->getValue()['value']))
      ];
    }
    return $header_array;
  }

  private function getSubmissionData(Survey $consultation_survey, $paginate=FALSE, $display_count=5) {
    // Responses is the term we're going to use to describe all the replies from a single respondent.  We use built in Drupal pager to grab  distinct responses.
    // It will also fetch the responses that we're going to display on this page.  Handy dandy.
    $connection = \Drupal::database();
    $response_query = $connection->select('consultation_answer', 'ca');
    $response_query->fields('ca', ['response_id']);
    $response_query->condition('survey', $consultation_survey->id(), '=');
    if($paginate) 
      return $response_query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit($display_count)->distinct()->execute();

      return $response_query->distinct()->execute();  // Non paginated results
    
  }

  private function buildSubmissionTable($response_query_results, Survey $consultation_survey, $question_ids) {
    // We get the response_ids and also set up the array bins for the table responses.  Each row of the table is an array with m elements, where m is the number of questions in the survey.  We fill all the elements with a blank placeholder to start with.
    $responses = [];
    $answer_rows = [];
    foreach($response_query_results as $response) {
      $responses[] = $response;
      $answer_rows[$response->response_id] = array_fill(0, count($question_ids) + 1, '-');
    }
    $responses = array_column($responses, 'response_id');
    // We only proceed if the current page has some responses to display
    if(count($responses) > 0 ) {  
      // Grab the answers, the only sort that's really necessary is the answer weight as we'll be using binning to sort the rest of the data
      $answer_query = \Drupal::entityQuery('consultation_answer')
                              ->condition('survey', $consultation_survey->id())
                              ->condition('response_id', $responses, 'IN')
                              ->sort('response_id', 'ASC')
                              ->sort('survey_question', 'ASC')
                              ->sort('answer_weight', 'ASC');

      $answer_ids = $answer_query->execute();
      $answers = SurveyAnswer::loadMultiple($answer_ids);
      
      foreach($answers as $answer) {
        // At time of writing deleted questions and surveys do not delete their associated answers.  Therefore we need to check this is an answer to a current/valid question
        // TODO: Option to delete answers when survey or question is deleted.
        if(($question_index = array_search($answer->get('survey_question')->getValue()[0]['target_id'], $question_ids, true)) !== FALSE) {
          $response_id = $answer->get('response_id')->getValue()[0]['value']; // This is the row index for the table for this answer
          $answer_text = $answer->get('answer_value')->getValue()[0]['value'];  // This is the column index for the table for this answer
          // We do some nasty content checking for the current answer in order to add commas for weighted/multiple response answers
          $answer_rows[$response_id][$question_index] = ($answer_rows[$response_id][$question_index] === '-' ? $answer_text : $answer_rows[$response_id][$question_index].','.$answer_text);
          $answer_rows[$response_id][count($question_ids)] = \Drupal::service('date.formatter')->format($answer->getChangedTime(), 'custom', 'd/m/Y H:i');
        }
      }
    }
    else 
      $answer_rows = [];
    return $answer_rows;
  }
}
