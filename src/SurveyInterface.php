<?php

namespace Drupal\consultation;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Survey entity.
 * @ingroup consultation
 */
interface SurveyInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}

?>