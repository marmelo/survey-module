<?php

namespace Drupal\consultation;

use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Question entity.
 * @ingroup consultation
 */
interface SurveyQuestionInterface extends  EntityOwnerInterface, EntityChangedInterface {

}

?>