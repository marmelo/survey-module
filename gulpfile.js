"use strict";

var gulp = require("gulp");
var sass = require("gulp-sass");

gulp.task('sass', function() {
  return gulp.src('sass/consultation.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('css'));
});

// second argument means sass is run as a dependency (ie first)
gulp.task('autoprefixer', ['sass'], function () {
  var postcss      = require('gulp-postcss');
  var sourcemaps   = require('gulp-sourcemaps');
  var autoprefixer = require('autoprefixer');
  var flexbug      = require('postcss-flexbugs-fixes');
  var urlencodesvg = require('gulp-css-urlencode-inline-svgs');

  return gulp.src('css/consultation.css')
      .pipe(sourcemaps.init())
      .pipe(postcss([ autoprefixer(), flexbug() ]))        
      .pipe(urlencodesvg())
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest('css'));
});

gulp.task('watch', function() {

  gulp.watch(['sass/*/*.scss', 'sass/*/*/*.scss', 'sass/*.scss'], ['autoprefixer']);

});

gulp.task('theme-init', function() {
  
  var util = require('gulp-util');
  var prompt = require('gulp-prompt');
  var replace = require('gulp-replace');
  var rename = require("gulp-rename");
  var del = require('del');
  var vinylPaths = require('vinyl-paths');


  return gulp.src('./gulpfile.js')
      .pipe(prompt.prompt([{
          type: 'input',
          name: 'themename',
          message: 'What is the machine name of your theme?'
      },{
        type: 'input',
        name: 'description',
        message: 'Give a short description of your theme'
      }, {
        type: 'input',
        name: 'name',
        message: 'What is the user-friendly name of your theme?'
      }], function(result) {

        util.log('Renaming this theme as: ' + util.colors.green(result.themename));
        util.log('Registering description: ' + util.colors.green(result.description));
        util.log('Titling theme as: ' + util.colors.green(result.name));        

        gulp.src(['BASE_THEME_REPLACE.info.yml', 'BASE_THEME_REPLACE.libraries.yml', 'BASE_THEME_REPLACE.theme'])
          .pipe(vinylPaths(del))
          .pipe(replace('BASE_THEME_REPLACE', result.themename))
          .pipe(replace('DESCRIPTION_REPLACE', result.description))
          .pipe(replace('NAME_REPLACE', result.name))
          .pipe(rename(function(path) {
            path.basename = path.basename.replace('BASE_THEME_REPLACE', result.themename);
          }))
          
          .pipe(gulp.dest('./'));                                    

      }));      
});

gulp.task("default", ["autoprefixer","watch"]);
